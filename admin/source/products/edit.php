<?php 
	if (empty($_GET['param'])) {
		header('location:/admin?action=products/list'); die();
	}
	$id = $_GET['param'];
	$sql = "SELECT * FROM products WHERE id=$id";
	$result = mysqli_query($con, $sql);
	if (mysqli_num_rows($result) <= 0) {
		header('location:/admin?action=products/list');die;
	}
	$result = mysqli_fetch_assoc($result);
	if (isset($_POST['submit'])) { 
		if ( isset($_POST['title']) && isset($_FILES['file']['name']) && isset($_POST['short_description']) && isset($_POST['price']) && isset($_POST['description']) ) {
			$title = $_POST['title'];
			$short_description = $_POST['short_description'];
			$price = $_POST['price'];
			$des = $_POST['description'];
			$is_feauture = 0;
			if ( isset($_POST['is_feauture']) ) {
				$is_feauture = 1;
			}
			if ($_FILES['file']['type'] == "image/jpeg" || $_FILES['file']['type'] == "image/png" || $_FILES['file']['type'] == "image/gif") {
				//kiểm tra xem file nhận vào có phải file ảnh hay k
				$path = "upload/products/";  // đường dẫn đến file upload
     			$tmp_name = $_FILES['file']['tmp_name']; //
     			$name = $_FILES['file']['name']; //
     			$avata = '/admin/'.$path.$name;
     			//upload file
     			move_uploaded_file($tmp_name, $path.$name);
     			
     			$sql = "UPDATE products SET title='$title', avata='$avata', short_description ='$short_description', price = '$price', description = '$des', is_feauture='$is_feauture' WHERE id = $id;" ;


				if (mysqli_query($con, $sql)) {
					echo "<script>";
					echo "alert('Sửa sản phẩm thành công');";
					echo "window.location.href='/admin?action=products/list';";
					echo "</script>";
				}else {
					echo "<script>";
					echo "alert('Lỗi: ".mysqli_error($con)."');";
					echo "</script>";
				}


			} else {
				echo "<script>";
				echo "alert('File upload phải là file ảnh');";
				echo "window.location.href='/admin?action=products/add';";
				echo "</script>";
			}
			
			
		}
	}
 ?>
<section class="content-header">
    <h1>
        Thêm sản phẩm
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>
<section class="content">
	<form action="" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="form-group">
					<label>Title</label>
				    <input type="text" class="form-control" name="title" required="">
				</div>

				<div class="form-group">
					<label>Avata</label>
					<input type="file" name="file" class="form-control" required="">
				</div>

				<div class="form-group">
					<label>Danh mục sản phẩm</label>
					<select name="category" required class="form-control">
						<option value="">----- Chọn danh mục sản phẩm -----</option>
						<?php 
							$sql = "SELECT id, title FROM product_categories";
							$result = mysqli_query($con, $sql);
							if (mysqli_num_rows($result) <= 0) {
								header('location:/admin?action=products/list');die;
							} else {
								while ($row = mysqli_fetch_assoc($result)) {
									echo "<option>". $row["title"]."</option>";
								}
							}
						 ?>
					</select>	
				</div>
				
				<div class="form-group">
					<label>Mô tả ngắn</label>
				    <textarea name="short_description" class="form-control"></textarea> 
				</div>
				
				<div class="form-group">
					<label>Giá sản phẩm</label>
				    <input type="number" class="form-control" name="price" required="">
				</div>

				<div class="form-group">
					<label>Nổi bật</label>
				    <input type="checkbox" class="" name="is_feauture" required="">
				</div>
				<div class="form-group">
					<label>Nội dung</label>
				    <textarea name="description" id="description" class="form-control"></textarea> 
				    
				</div>
				   
				<div class="text-right">
					<button type="submit" class="btn btn-primary" name="submit">Edit</button>
				    <button type="reset" class="btn btn-default" name="reset">Reset</button>
				    
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</form>
</section>

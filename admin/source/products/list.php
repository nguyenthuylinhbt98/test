<section class="content-header">
    <h1>
        Danh sách sản phẩm
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Danh sách sản phẩm</li>
    </ol>
</section>
<section class="content">
	<div class="box-body">
	    <table id="example2" class="table table-bootsrap table-hover">
	        <thead>
	            <tr>
	                <th>STT</th>
	                <th>Avata</th>
	                <th>Tên sản phẩm</th>
	                <th>Mô tả ngắn</th>
	                <th>Giá sản phẩm</th>
	                <th></th>
	                <th></th>
	            </tr>
	        </thead>
	        <tbody>
	        	<?php  
	        		$sql = "SELECT * FROM products ORDER BY id DESC";
	        		$result = mysqli_query($con, $sql);
	        		if (mysqli_num_rows($result) > 0) {
	        			$i = 1;
	        			while ( $row = mysqli_fetch_assoc($result) ) {
	        	?>
	        				<tr>
				                <td><?php echo $i; ?></td>
				                <td><img src="<?php echo $row['avata']; ?>" alt="" style="width: 150px; height: 150px"></td>
				                <td><?php echo $row['title']; ?></td>
				                <td><?php echo $row['short_description']; ?></td>
				                <td><?php echo $row['price']; ?></td>
				                <td>
				                	<a href="/admin?action=products/edit&param=<?php echo $row['id']; ?>" class="btn btn-success" title="Sửa">Sửa</a>
				                </td>
				                <td>
				                	<a href="/admin?action=products/delete&param=<?php echo $row['id']; ?>" class="btn btn-danger" title="Xóa">Xóa</a>
				                </td>
				            </tr>
	        	<?php
	        				$i++;
	        			}
	        		}
	        	?>
	            
	            
	        </tbody>
	    </table>
	</div>
</section>
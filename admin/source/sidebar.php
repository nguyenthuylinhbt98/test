<ul class="sidebar-menu" data-widget="tree">
    <li class="header">MAIN NAVIGATION</li>
    <li >
        <a href="/admin">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>     
        </a>
    </li>
    <li class="treeview">
        <a href="#">
        <i class="fa fa-files-o"></i>
        <span>Product categories</span>
        <span class="pull-right-container">
        <span class="label label-primary pull-right">2</span>
        </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="/admin?action=product_categories/add"><i class="fa fa-circle-o"></i> Thêm danh mục </a></li>
            <li><a href="/admin?action=product_categories/list"><i class="fa fa-circle-o"></i> Danh sách</a></li>
        </ul>
    </li>

    <li class="treeview">
        <a href="#">
        <i class="fa fa-files-o"></i>
        <span> Product </span>
        <span class="pull-right-container">
        <span class="label label-primary pull-right">2</span>
        </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="/admin?action=products/add"><i class="fa fa-circle-o"></i> Thêm sản phẩm </a></li>
            <li><a href="/admin?action=products/list"><i class="fa fa-circle-o"></i> Danh sách</a></li>
        </ul>
    </li>

    <li class="treeview">
        <a href="#">
        <i class="fa fa-files-o"></i>
        <span>Project</span>
        <span class="pull-right-container">
        <span class="label label-primary pull-right">2</span>
        </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="/admin?action=projects/add"><i class="fa fa-circle-o"></i> Thêm project </a></li>
            <li><a href="/admin?action=projects/list"><i class="fa fa-circle-o"></i> Danh sách</a></li>
        </ul>
    </li>

    <li class="treeview">
        <a href="#">
        <i class="fa fa-files-o"></i>
        <span>Users</span>
        <span class="pull-right-container">
        <span class="label label-primary pull-right">2</span>
        </span>
        </a>
        <ul class="treeview-menu">
            <li><a href="/admin?action=users/add"><i class="fa fa-circle-o"></i> Thêm user </a></li>
            <li><a href="/admin?action=users/list"><i class="fa fa-circle-o"></i> Danh sách</a></li>
        </ul>
    </li>
</ul>
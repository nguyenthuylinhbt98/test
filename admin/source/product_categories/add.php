<?php 
	if (isset($_POST['submit'])) {
		if (isset($_POST['title']) && isset($_POST['slug']) && isset($_POST['description']) && isset($_POST['order'])) {
			$title = $_POST['title'];
			$slug = $_POST['slug'];
			$des = $_POST['description'];
			$order = $_POST['order'];
			$sql = "INSERT INTO product_categories (title, slug, description, order_by) VALUES ('$title', '$slug', '$des', '$order');";
			if (mysqli_query($con, $sql)) {
				echo "<script>";
				echo "alert('Thêm danh mục sản phẩm thành công');";
				echo "window.location.href='/admin?action=product_categories/list';";
				echo "</script>";
			}else {
				echo "<script>";
				echo "alert('Lỗi: ".mysqli_error($con)."');";
				echo "</script>";
			}
		}
	}
 ?>
<section class="content-header">
    <h1>
        Thêm danh mục sản phẩm
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>
<section class="content">
	<form action="" method="post" enctype="multipart/from_data">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="form-group">
					<label>Tiêu đề</label>
					<input type="text" class="form-control" name="title" required>
				</div>
				<div class="form-group">
					<label>Tên không dấu</label>
					<input type="text" class="form-control" name="slug" required>
				</div>
				<div class="form-group">
					<label>Order by</label>
					<input type="number" class="form-control" name="order" required>
				</div>
				<div class="form-group">
					<label>Mô tả</label>
					<textarea name="description"  class="form-control"></textarea>
				</div>
				<div class="text-right">
					<button class="btn btn-primary" type="submit" name="submit">Add</button>
					<button class="btn btn-default" type="reset">Reset</button>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</form>
</section>    
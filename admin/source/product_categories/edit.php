<?php 
	if ( empty($_GET['param']) ) {
		header('location:/admin?action=product_categories/list');die;
	}
	$id = $_GET['param'];
	$sql = "SELECT * FROM product_categories WHERE id=$id";
	$result = mysqli_query($con, $sql);
	if (mysqli_num_rows($result) <= 0) {
		header('location:/admin?action=product_categories/list');die;
	}
	$result = mysqli_fetch_assoc($result);
	
	if (isset($_POST['submit'])) {
		if ( isset($_POST['title']) && isset($_POST['slug']) && isset($_POST['description']) && isset($_POST['order'])) {
			$title = $_POST['title'];
			$slug = $_POST['slug'];
			$des = $_POST['description'];
			$order = $_POST['order'];
			$sql = "UPDATE product_categories SET title='$title' , slug='$slug', description='$des' , order_by='$order' WHERE id=$id;";
			if (mysqli_query($con, $sql)) {
				echo "<script>";
				echo "alert('Sửa danh mục sản phẩm thành công');";
				echo "window.location.href='/admin?action=product_catelogies/list';";
				echo "</script>";
			}else {
				echo "<script>";
				echo "alert('Lỗi: ".mysqli_error($con)."');";
				echo "</script>";
			}
		}
	}
 ?>
<section class="content-header">
    <h1>
        Sửa danh mục sản phẩm
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>
<section class="content">
	<form action="" method="post" enctype="multipart/from_data">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="form-group">
					<label>Tiêu đề</label>
					<input type="text" class="form-control" name="title" required value="<?php echo $result['title'];?>">
				</div>
				<div class="form-group">
					<label>Tên không dấu</label>
					<input type="text" class="form-control" name="slug" required value="<?php echo $result['slug'];?>">
				</div>
				<div class="form-group">
					<label>Order by</label>
					<input type="number" class="form-control" name="order" required value="<?php echo $result['order'];?>">
				</div>
				<div class="form-group">
					<label>Mô tả</label>
					<textarea name="description" class="form-control" value="<?php echo $result['description'];?>"></textarea>
				</div>
				<div class="text-right">
					<button class="btn btn-primary" type="submit" name="submit">Edit</button>
					<button class="btn btn-default" type="reset">Reset</button>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</form>
</section>    
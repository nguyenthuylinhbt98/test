<section class="content-header">
    <h1>
        Danh sách quản trị viên
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Danh sách quản trị viên</li>
    </ol>
</section>
<section class="content">
	<div class="box-body">
	    <table id="example2" class="table table-bootsrap table-hover">
	        <thead>
	            <tr>
	                <th>STT</th>
	                <th>Username</th>
	                <th>Quyền quản trị</th>
	                <th></th>
	                <th></th>
	            </tr>
	        </thead>
	        <tbody>
	        	<?php  
	        		$sql = "SELECT * FROM users ORDER BY id DESC";
	        		$result = mysqli_query($con, $sql);
	        		if (mysqli_num_rows($result) > 0) {
	        			$i = 1;
	        			while ( $row = mysqli_fetch_assoc($result) ) {
	        	?>
	        				<tr>
				                <td><?php echo $i; ?></td>
				                <td><?php echo $row['username']; ?></td>
				                <td><?php echo $row['role']; ?></td>
				                <td>
				                	<a href="/admin?action=users/edit&param=<?php echo $row['id']; ?>" class="btn btn-success" title="Sửa">Sửa</a>
				                </td>
				                <td>
				                	<a href="/admin?action=users/delete&param=<?php echo $row['id']; ?>" class="btn btn-danger" title="Xóa">Xóa</a>
				                </td>
				            </tr>
	        	<?php
	        				$i++;
	        			}
	        		}
	        	?>
	            
	            
	        </tbody>
	    </table>
	</div>
</section>
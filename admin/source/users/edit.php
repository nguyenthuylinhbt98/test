<?php 

	if ( empty($_GET['param']) ) {
		header('location:/admin?action=users/list');die;
	}
	$id = $_GET['param'];
	$sql = "SELECT * FROM users WHERE id=$id";
	$result = mysqli_query($con, $sql);
	if (mysqli_num_rows($result) <= 0) {
		header('location:/admin?action=users/list');die;
	}
	$result = mysqli_fetch_assoc($result);
	if (isset($_POST['submit'])) {
		if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['role'])) {
			$name = $_POST['username'];
			$password = $_POST['password'];
			$role = $_POST['role'];
			$sql = "UPDATE users SET username='$name', password='$password', role='$role' WHERE id=$id";
			if (mysqli_query($con, $sql)) {
				echo "<script>";
				echo "alert('Sửa user thành công');";
				echo "window.location.href='/admin?action=users/list';";
				echo "</script>";
			}else {
				echo "<script>";
				echo "alert('Lỗi: ".mysqli_error($con)."');";
				echo "</script>";
			}
		}
	}
 ?>
<section class="content-header">
    <h1>
        Thêm user
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Thêm user</li>
    </ol>
</section>
<section class="content">
	<form action="" method="post" enctype="multipart/from_data">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="form-group">
					<label>Username</label>
					<input type="text" class="form-control" name="username" required>
				</div>
				<div class="form-group">
					<label>Password</label>
					<input type="password" class="form-control" name="password" required>
				</div>
				<div class="form-group">
					<label>Role</label>
					<input type="number" class="form-control" name="role" required>
				</div>
				<div class="text-right">
					<button class="btn btn-primary" type="submit" name="submit">Edit</button>
					<button class="btn btn-default" type="reset">Reset</button>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</form>
</section>    
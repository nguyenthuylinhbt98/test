<?php
	if (empty($_GET['param'])) {
		header('location:/admin?action=projects/list'); die();
	}
	$id = $_GET['param'];
	$sql = "SELECT * FROM projects WHERE id=$id";
	$result = mysqli_query($con, $sql);
	if (mysqli_num_rows($result) <= 0) {
		header('location:/admin?action=projects/list');die;
	}
	$result = mysqli_fetch_assoc($result); 
	if (isset($_POST['submit'])) { 
		if ( isset($_POST['name']) && isset($_FILES['file']['name']) ) {
			$name = $_POST['name'];
			
			if ($_FILES['file']['type'] == "image/jpeg" || $_FILES['file']['type'] == "image/png" || $_FILES['file']['type'] == "image/gif") {
				//kiểm tra xem file nhận vào có phải file ảnh hay k
				$path = "upload/projects/";  // đường dẫn đến file upload
     			$tmp_name = $_FILES['file']['tmp_name']; //
     			$name = $_FILES['file']['name']; //
     			$avata = '/admin/'.$path.$name;
     			//upload file
     			move_uploaded_file($tmp_name, $path.$name);
     			
     			$sql = "UPDATE projects SET name='$name', avata='$avata'  WHERE id = $id;";


				if (mysqli_query($con, $sql)) {
					echo "<script>";
					echo "alert('Sửa project thành công');";
					echo "window.location.href='/admin?action=projects/list';";
					echo "</script>";
				}else {
					echo "<script>";
					echo "alert('Lỗi: ".mysqli_error($con)."');";
					echo "</script>";
				}


			} else {
				echo "<script>";
				echo "alert('File upload phải là file ảnh');";
				echo "window.location.href='/admin?action=projects/add';";
				echo "</script>";
			}
			
			
		}
	}
 ?>
<section class="content-header">
    <h1>
        Thêm sản phẩm
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>
<section class="content">
	<form action="" method="post" enctype="multipart/form-data">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="form-group">
					<label>Tên project</label>
				    <input type="text" class="form-control" name="name" required="">
				</div>

				<div class="form-group">
					<label>Avata</label>
					<input type="file" name="file" class="form-control" required="">
					
				</div>

				<div class="text-right">
					<button type="submit" class="btn btn-primary" name="submit">Edit</button>
				    <button type="reset" class="btn btn-default" name="reset">Reset</button>
				    
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</form>
</section>

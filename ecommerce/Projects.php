
    <div class="container">
        <div class="home">
            <a href="Home.html" title=""><i class="fas fa-home"></i></a>
            <i class="fas fa-angle-right">Projects</i>
        </div>
    </div>

    <!-- content -->
    <div class="content">
    	<div class="container">
            <?php 
                $sql = "SELECT * FROM Projects";
                $result = mysqli_query($con, $sql);
                if ( mysqli_num_rows( $result) >0 ) {
                    while ($row = mysqli_fetch_assoc($result)) {
            ?>
            <div class="project">
                <div class="image">
                    <img src="<?php echo($row['avata'])  ?>" alt="">
                </div>
                <div class="note">
                    <p class="name"><?php echo ($row['name']); ?></p>
                    
                </div>              
            </div>

            <?php
                    }
                }
             ?>
    		
    	</div>	
    </div>

	


    <div class="content ">
        <div class="container ">
            <div class="row ">
                <div class="col-md-3 ">
                    <div class="left-content">  
                        <div class="left-menu">
                            
                                <?php include ("source/sidebar.php") ?>
                            
                        </div>
                        <div class="left-poster">
                            <img src="assets/images/Home/left-image.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="right-content">
                        <div class="feature-product wow flipInX" data-wow-delay="0.5s" >
                            <h3>FEATURE  PRODUCT</h3>
                            <?php include("source/feature-product.php") ?>
                            
                            <div class="clear"></div>
                            <div class="btn-viewall">
                                <button type="btn" class="btn btn-danger btn-lg">view all <i class="fas fa-angle-right"></i> </button>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="right-poster hidden-xs hidden-sm">

                            <div class="poster" id="p1">
                                <img src="assets/images/Home/pic1.png" alt="">
                            </div>
                            <div class="poster">
                                <img src="assets/images/Home/pic2.png" alt="">
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="new-product  wow flipInX" data-wow-delay="0.5s">
                            <h3>NEW PRODUCT</h3>
                            <div class="product">
                                <div class="image-product">
                                    <img src="assets/images/Home/5.png" alt="">
                                </div>
                                <div class="infor-product">
                                    <p class="name">MP4102 – Memo Desk Set</p>
                                    <p class="price">$10</p>
                                    <button type="button" class="btn btn-default">Add to Enquiry</button>
                                </div>
                            </div>
                            <div class="product">
                                <div class="image-product">
                                    <img src="assets/images/Home/6.png" alt="">
                                </div>
                                <div class="infor-product">
                                    <p class="name">MP4102 – Memo Desk Set</p>
                                    <p class="price">$10</p>
                                    <button type="button" class="btn btn-default">Add to Enquiry</button>
                                </div>
                            </div>
                            <div class="product">
                                <div class="image-product">
                                    <img src="assets/images/Home/7.png" alt="">
                                </div>
                                <div class="infor-product">
                                    <p class="name">MP4102 – Memo Desk Set</p>
                                    <p class="price">$10</p>
                                    <button type="button" class="btn btn-default">Add to Enquiry</button>
                                </div>
                            </div>
                            <div class="product">
                                <div class="image-product">
                                    <img src="assets/images/Home/8.png" alt="">
                                </div>
                                <div class="infor-product">
                                    <p class="name">MP4102 – Memo Desk Set</p>
                                    <p class="price">$10</p>
                                    <button type="button" class="btn btn-default">Add to Enquiry</button>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="btn-viewall">
                                <button type="btn" class="btn btn-danger btn-lg">view all <i class="fas fa-angle-right"></i> </button>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="gift  wow flipInX" data-wow-delay="0.5s">
                            <h3>GIFT SET & BASKETS</h3>
                            <div class="product">
                                <div class="image-product">
                                    <img src="assets/images/Home/gif1.png" alt="">
                                </div>
                                <div class="infor-product">
                                    <p class="name">MP4102 – Memo Desk Set</p>
                                    <p class="price">$10</p>
                                    <button type="button" class="btn btn-default">Add to Enquiry</button>
                                </div>
                            </div>
                            <div class="product">
                                <div class="image-product">
                                    <img src="assets/images/Home/gif1.png" alt="">
                                </div>
                                <div class="infor-product">
                                    <p class="name">MP4102 – Memo Desk Set</p>
                                    <p class="price">$10</p>
                                    <button type="button" class="btn btn-default">Add to Enquiry</button>
                                </div>
                            </div>
                            <div class="product">
                                <div class="image-product">
                                    <img src="assets/images/Home/gif1.png" alt="">
                                </div>
                                <div class="infor-product">
                                    <p class="name">MP4102 – Memo Desk Set</p>
                                    <p class="price">$10</p>
                                    <button type="button" class="btn btn-default">Add to Enquiry</button>
                                </div>
                            </div>
                            <div class="product">
                                <div class="image-product">
                                    <img src="assets/images/Home/gif1.png" alt="">
                                </div>
                                <div class="infor-product">
                                    <p class="name">MP4102 – Memo Desk Set</p>
                                    <p class="price">$10</p>
                                    <button type="button" class="btn btn-default">Add to Enquiry</button>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="btn-viewall">
                                <button type="btn" class="btn btn-danger btn-lg">view all <i class="fas fa-angle-right"></i> </button>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="home">
            <a href="Home.html" title=""><i class="fas fa-home"></i></a>
            <i class="fas fa-angle-right">Our Products</i>
        </div>
    </div>
    <!-- content -->
    <div class="content">
    	<div class="container">
    		<div class="row">
    			<div class="col-md-3 ">
                    <div class="left-content ">
                        <div class="left-menu">
                            <ul>
                                <?php include('source/sidebar.php') ?>
                            </ul>
                            
                        </div>
                        <div class="left-poster">
                            <img src="assets/images/Home/left-image.png" alt="">
                        </div>
                    </div>
                </div>
    			<div class="col-md-9">
    				<div class="right-content">
                        <div class="our-product">
                            <h3>our product</h3>
                            <?php
                                $sql = "SELECT * FROM Products";
                                $result = mysqli_query($con, $sql);
                                if (mysqli_num_rows($result) >0) {
                                while ($row = mysqli_fetch_assoc($result)) {
                            
                             
                            ?>

                        <div class="product">
                            <a href="/ecommerce?action=Induavial-Our Products&param=<?php echo $row['id']; ?>" title="">
                                <div class="image-product" >
                                    <img src="<?php echo($row['avata'])  ?>" alt="">
                                </div>
                            </a>
                        <div class="infor-product">
                            <a href="/ecommerce?action=Induavial-Our Products&param=<?php echo $row['id']; ?>" title="">
                                <p class="name"><?php echo ($row['title']); ?></p>
                            </a>
                            <p class="price"><?php echo "$". $row["price"];  ?></p>
                            <form action="/display/souce/add-enquiry.php" method="POST" accept-charset="utf-8">
                                <input type="hidden" name="product_id" value="<?php echo $row['id']; ?>">
                                <button type="button" class="btn btn-default add_enquiry">Add to Enquiry</button>
                            </form>
                        </div>
                    </div>
                            <?php
            
                                         }
                                    }
                            ?>
                    <div  style="clear: both;">
                                
                            </div>        
                            <div class="our-btn">
                            	<button type="btn" class="btn btn-danger btn-lg">Read more </button>
                            	
                            </div>

                            
                        </div>
                    </div>
    			</div>
    		</div>
    		
    	</div>	
    </div>

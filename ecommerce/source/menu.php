<div class="col-md-9 col-sm-7 col-xs-2">
    <button type="button" class="menu-bars"><i class="fas fa-bars"></i></button>
    <ul class="main-menu">
        <li>
            <a href="/ecommerce?action=Home" title=" ">HOME</a>
        </li>
        <li>
            <a href="/ecommerce?action=About_us" title=" ">ABOUT US</a>
        </li>
        <li>
            <a href="/ecommerce?action=Projects" title=" ">PROJECT</a>
        </li>
        <li>
            <a href="/ecommerce?action=Our Products" title=" ">OUR PRODUCTS <i class="fas fa-angle-down "></i></a>
        </li>
        <li>
            <a href="# " title=" ">TESTIMONIAL</a>
        </li>
        <li>
            <a href="/ecommerce?action=Contact Us" title=" ">CONTACT US</a>
        </li>
        <div class="clear"></div>
    </ul>
</div>
<div class="col-md-3 col-sm-5 col-xs-10">
    <div class="search ">
        <form>
            <input type="text " name="Search " placeholder=" Search ">
            <button type="submit "><i class="fas fa-search "></i></button>
        </form>
    </div>
</div>

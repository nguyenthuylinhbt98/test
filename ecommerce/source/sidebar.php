
 <ul class="product-categories">
    <li class= "product-categories-mobile-button">
        <p>Product Categories <i class="fas fa-angle-down"></i></p>        
    </li>

    <?php 
        $sql = "SELECT title FROM product_categories";
        $result = mysqli_query($con, $sql);
        if (mysqli_num_rows($result) <= 0) {
        
        } else {
            while ($row = mysqli_fetch_assoc($result)) {
        
        ?>
    <li class="sidebar-item">
        <a href="#" title=""><?php echo($row["title"]) ?></a>
        <i class="fas fa-angle-right"></i>
    </li>
    <?php
            }
        }       
    ?>
</ul>
<script>
    $('.product-categories-mobile-button').click(function(){
        $('.sidebar-item').toggleClass('open-menu');
    });

</script>
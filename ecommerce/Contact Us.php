 
    <div class="container">
        <div class="home">
            <a href="Home.html" title=""><i class="fas fa-home"></i></a>
            <i class="fas fa-angle-right">Contact us</i>
        </div>
    </div>
    <!-- content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="contact">
                        <div class="store-infor">
                            <h3>Get in touch with us</h3>
                            <p><i class="fas fa-map-marker-alt"></i>96C Jalan Senang Singapore 418483.</p>
                            <p><i class="fas fa-envelope"></i>sales@impressgift.com.sg</p>
                            <p><i class="fas fa-phone"></i>+65 6876 0079</p>
                        </div>
                       <form action="/ecommerce/" method="post" accept-charset="utf-8">
                            <div class="guess-infor">
                                <input class="top" id="first-name" placeholder="First Name:">
                                <input class="top" id="name" placeholder="Last Name:">
                                <input class="top" id="email" placeholder="Email:">
                                <input class="top" id="phone" placeholder="Phone:">
                                <input class="bottom" placeholder="Request Detail:">
                                <button type="submit">Submit request</button>
                            </div>    
                       </form>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="place">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.677531287161!2d105.84127411427663!3d21.00555959394728!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac76ccab6dd7%3A0x55e92a5b07a97d03!2zVHLGsOG7nW5nIMSQ4bqhaSBo4buNYyBCw6FjaCBraG9hIEjDoCBO4buZaQ!5e0!3m2!1svi!2s!4v1533496508558" width="100%" height="450" frameborder="0"  style="border:0;margin-top: 75px;" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- footer -->
   
<?php 
    include ('connect.php');
    session_start();

    if (isset($_GET['action'])) {
        $redirect=$_GET['action'];
    }else {
        $redirect = 'Home';
    }
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=vietnamese" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Charmonman|Lobster&amp;subset=cyrillic,cyrillic-ext,latin-ext,vietnamese" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    
</head>

<body>
    <!-- header -->
    <header id="header" class="wow bounceIn">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-2 col-xs-5">
                    <div class="logo">
                        <a href="Home.html"><img src="assets/images/Home/logo.png" alt=""></a>
                    </div>
                </div>
                <div class="col-md-8 col-sm-10 col-xs-7">
                    <div class="information">
                        <div class="headerinfor hidden-xs" id="ship">
                            <div class="symbol">
                                <img src="assets/images/Home/oto.png" alt="">
                            </div>
                            <div class="infor wow bounceInLef">
                                <h4>FREE SHIPPING</h4>
                                <p>Free shipping on all order</p>
                            </div>
                        </div>
                        <div class="headerinfor hidden-xs" id="phone">
                            <div class="symbol">
                                <img src="assets/images/Home/phone.png " alt=" ">
                            </div>
                            <div class="infor">
                                <h4>HOTLINE</h4>
                                <p>+65 6876 0079</p>
                            </div>
                        </div>
                        
                        <a href="/ecommerce?action=ENquiry" title="">
                            <div class="headerinfor ">
                                <div class="symbol">
                                    <img src="assets/images/Home/giohang.png " alt=" ">
                                </div>
                                <div class="infor">
                                    <h4>Enquiry Cart</h4>
                                    <p>
                                        (<?php if( isset($_SESSION['enquiry']) ) echo count($_SESSION['enquiry']); else echo 0; ?>)
                                    </p>
                                </div>
                            </div>
                        </a>

                        <div class="clear">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- /header -->
    <!-- menu -->
    <div class="menu">
        <div class="container ">
            <div class="row">
                <?php include("source/menu.php") ?>
            </div>
        </div>
    </div>
    <!-- content -->
    <div class="content ">
        <div class="container ">
            <div class="row ">
            <?php include($redirect.".php") ?>
            </div>
        </div>
    </div>
    <!-- footer -->
    <div class="footer ">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-sm-2">
                    <div class="image">
                        <img src="assets/images/Home/footer-logo.png" alt="">
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="impressgift">
                        <h4>IMPRESSGIFT</h4>
                        <img src="assets/images/Home/line.png" alt="">
                        <p><i class="fas fa-map-marker-alt"></i> 96C Jalan Senang Singapore 418483.</p>
                        <p><i class="fas fa-phone-volume"></i> +65 6876 0079</p>
                        <p><i class="fas fa-envelope"></i> sales@impressgift.com.sg</p>
                        <p><i class="far fa-clock"></i> <b>Hour Opening:</b><br><p class="indent"> Mon-Sun: 8:00am - 10:00pm</p></p>
                    </div>
                </div>
                <div class="col-md-2 hidden-sm hidden-xs">
                    <div class="quick-link">
                        <h4>quick link</h4>
                        <img src="assets/images/Home/line.png" alt="">
                        <p>Home</p>
                        <p>About us</p>
                        <p>Projects</p>
                        <p>Collection</p>
                        <p>Contact us</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="signup">
                        <h4>Signup for our newsletter</h4>
                        <img src="assets/images/Home/line.png" alt="">
                        <p>Receive our latest updates about our products and promotions</p>
                        <input class="inp1" placeholder="Enter your email address">
                        <input class="inp2" value="SUBSCRIBE" type="submit">
                    </div>
                    <div class="connected">
                        <h4>stay connected</h4>
                        <img src="assets/images/Home/fb.png" alt="">
                        <img src="assets/images/Home/gg.png" alt="">
                        <img src="assets/images/Home/is.png" alt="">
                        <img src="assets/images/Home/yt.png" alt="">
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- end -->
    <div class="end">
        <p>Copyright © 2018 Impressgift. All Right Reserved.</p>
    </div>
    <script src="assets/js/jquery-3.3.1.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/app.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>
</body>

</html>
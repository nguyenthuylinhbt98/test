     <div class="container">
        <div class="home">
            <a href="Home.html" title=""><i class="fas fa-home"></i></a>
            <i class="fas fa-angle-right">Induavial-Our Products</i>
        </div>
    </div>
    <!-- content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="left-content ">
                        <div class="left-menu ">
                            <ul>
                                <?php include ("source/sidebar.php") ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="right-content">
                        <div class="note-book">
                            <?php 
                                $id = $_GET['param'];
                                $sql = "SELECT * FROM products WHERE id=$id";
                                $result = mysqli_query($con, $sql);
                                if (mysqli_num_rows($result) > 0) {
                                    while ($row = mysqli_fetch_assoc($result)) {
                            ?>
                            <div class="image-note-book">
                                    <img src="<?php echo $row['avata']; ?>" alt="">
                            </div>
                             <div class="infor-note-book">
                                    <h3><?php echo $row['title']; ?></h3>
                                    <p><?php echo $row['short_description'] ; ?></p>
                                    <p class="price"><?php echo "$".$row['price'] ; ?></p>
                                    <button type="button" class="btn btn-default btn-lg" id="lg-btn">Enquiry Now</button>

                                    <br><button type="button" class="btn btn-default" id="sm-btn1"><i class="fab fa-facebook-f"></i>Facebook</button>
                                    <button type="button" class="btn btn-default" id="sm-btn2"><i class="fab fa-twitter"></i>Twitter</button>
                                    <button type="button" class="btn btn-default" id="sm-btn3"><i class="fab fa-google-plus-g"></i>  Google+</button>
                                    <button type="button" class="btn btn-default" id="sm-btn4"><i class="fab fa-pinterest-p"></i> Pinterest</button>
                            </div>
                            <div style="clear: both;">
                                
                            </div>
                        </div>

                        <div class="description">
                            <h3>Description</h3>
                            <p class="main"><?php echo $row['description']; ?></p>
                        </div>



                            <?php
                                    }
                                }
                            ?>
                            
                           
                        <div class="related-products">
                            <h3>related products</h3>
                            <div class="product">
                                <div class="image-product">
                                    <img src="assets/images/Home/1.png" alt="">
                                </div>
                                <div class="infor-product">
                                    <p class="name">MP4102 – Memo Desk Set</p>
                                    <p class="price">$10</p>
                                    <button type="button" class="btn btn-default">Add to Enquiry</button>
                                </div>
                            </div>
                            <div class="product">
                                <div class="image-product">
                                    <img src="assets/images/Home/2.png" alt="">
                                </div>
                                <div class="infor-product">
                                    <p class="name">MP4102 – Memo Desk Set</p>
                                    <p class="price">$10</p>
                                    <button type="button" class="btn btn-default">Add to Enquiry</button>
                                </div>
                            </div>
                            <div class="product">
                                <div class="image-product">
                                    <img src="assets/images/Home/3.png" alt="">
                                </div>
                                <div class="infor-product">
                                    <p class="name">MP4102 – Memo Desk Set</p>
                                    <p class="price">$10</p>
                                    <button type="button" class="btn btn-default">Add to Enquiry</button>
                                </div>
                            </div>
                            <div class="product">
                                <div class="image-product">
                                    <img src="assets/images/Home/4.png" alt="">
                                </div>
                                <div class="infor-product">
                                    <p class="name">MP4102 – Memo Desk Set</p>
                                    <p class="price">$10</p>
                                    <button type="button" class="btn btn-default">Add to Enquiry</button>
                                </div>
                            </div>
                            <div class="product">
                                <div class="image-product">
                                    <img src="assets/images/Home/5.png" alt="">
                                </div>
                                <div class="infor-product">
                                    <p class="name">MP4102 – Memo Desk Set</p>
                                    <p class="price">$10</p>
                                    <button type="button" class="btn btn-default">Add to Enquiry</button>
                                </div>
                            </div>
                            <div class="product">
                                <div class="image-product">
                                    <img src="assets/images/Home/6.png" alt="">
                                </div>
                                <div class="infor-product">
                                    <p class="name">MP4102 – Memo Desk Set</p>
                                    <p class="price">$10</p>
                                    <button type="button" class="btn btn-default">Add to Enquiry</button>
                                </div>
                            </div>
                            <div class="product">
                                <div class="image-product">
                                    <img src="assets/images/Home/7.png" alt="">
                                </div>
                                <div class="infor-product">
                                    <p class="name">MP4102 – Memo Desk Set</p>
                                    <p class="price">$10</p>
                                    <button type="button" class="btn btn-default">Add to Enquiry</button>
                                </div>
                            </div>
                            <div class="product">
                                <div class="image-product">
                                    <img src="assets/images/Home/8.png" alt="">
                                </div>
                                <div class="infor-product">
                                    <p class="name">MP4102 – Memo Desk Set</p>
                                    <p class="price">$10</p>
                                    <button type="button" class="btn btn-default">Add to Enquiry</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>